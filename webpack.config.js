const path = require('path');
module.exports = {
    entry: {
      home : './public/scripts/home.js',
      user: './public/scripts/users/user.js'
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: "[name]-bundle.js"
    },
    module: {
         loaders: [{
             test: /\.js$/,
             exclude: /node_modules/,
             loader: 'babel-loader',
             query: {
               presets: ['es2015']
             }
         }]
     }
};
