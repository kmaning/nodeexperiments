import {User} from './model';
class usersFactory {

  getUsers() {
    let user1 = new User('001', 'kmaning', 'pass123', 'Kevin', 'Maning', 21);
    let user2 = new User('002', 'rplatino', 'pass234', 'Ramoncito', 'Platino', 21);
    let user3 = new User('003', 'jopinion', 'pass456', 'Judy', 'Opinion', 21);

    let users = [user1, user2, user3];
    
    return users;
  }

}

export {usersFactory};
