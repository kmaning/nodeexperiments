var express = require('express');
var app = express();
var path = require('path');

var PORT = process.env.PORT || 3000;

//express static setup
app.use('/styles', express.static('public/css'));
app.use('/scripts', express.static('public/scripts'));
app.use('/builds', express.static('build'));

//view engine setup
app.set('view engine', 'ejs');

//routes
require('./routes/home')(app);
require('./routes/user')(app);

app.listen(PORT);
