class User {
  constructor(Id, userName, password, firstName, lastName, age) {
    this.Id = Id;
    this.userName = userName;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  getFullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}

export {User};
