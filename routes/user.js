module.exports = (app) => {

  app.get('/api/user', (req, res)=>{
    res.render('user/userHome');
  });

  app.get('/api/user/:id', (req, res)=>{
    console.log(req.params['id']);
    res.end();
  });

  app.get('/user/greet/:name', (req, res) => {
    res.render('user/greetuser', {name:req.params['name']});
  });
};
